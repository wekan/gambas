#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form 3.15.90\n"
"POT-Creation-Date: 2024-09-16 12:46 UTC\n"
"PO-Revision-Date: 2021-02-17 15:40 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "More controls for graphical components"
msgstr "用于图形组件的更多控件"

#: CBookmarkList.class:51
msgid "Home"
msgstr "居家"

#: CBookmarkList.class:53
msgid "Desktop"
msgstr "桌面"

#: CBookmarkList.class:55
msgid "System"
msgstr "系统"

#: ColorPalette.class:185
msgid "Last colors"
msgstr "最近颜色"

#: ColorPalette.class:317
msgid "Remove color"
msgstr "移除颜色"

#: ColorPalette.class:321
msgid "Remove all colors"
msgstr "移除全部颜色"

#: ColorPalette.class:325
msgid "Sort colors"
msgstr "颜色排序"

#: DirView.class:597
msgid "Cannot rename directory."
msgstr "不能重命名目录。"

#: DirView.class:627
msgid "New folder"
msgstr "新建文件夹"

#: DirView.class:646
msgid "Cannot create directory."
msgstr "不能创建目录。"

#: FBugFileView.form:32
msgid "Toggle Fileview Detailed View"
msgstr ""

#: FBugFileView.form:37
msgid "Toggle Filechooser Detailed View"
msgstr ""

#: FCalendar.form:48
msgid "Today"
msgstr "今天"

#: FCalendar.form:54
msgid "Previous month"
msgstr "上一月"

#: FCalendar.form:60
msgid "Next month"
msgstr "下一月"

#: FCalendar.form:148
msgid "Apply"
msgstr "应用"

#: FColorChooser.form:81
msgid "Follow color grid"
msgstr "追随网格内颜色"

#: FDirChooser.class:441
msgid "Directory not found."
msgstr "目录未找到。"

#: FDirChooser.class:550
msgid "All files (*)"
msgstr "所有文件(*)"

#: FDirChooser.class:911
msgid "Overwrite"
msgstr "覆盖"

#: FDirChooser.class:911
msgid ""
"This file already exists.\n"
"\n"
"Do you want to overwrite it?"
msgstr ""
"这个文件已经存在。\n"
"\n"
"是否确定覆盖它？"

#: FDirChooser.class:1029
msgid "&Bookmark current directory"
msgstr "将当前目录加入书签(&B)"

#: FDirChooser.class:1037
msgid "&Edit bookmarks..."
msgstr "编辑书签...(&E)"

#: FDirChooser.class:1081
msgid "Preview big files"
msgstr ""

#: FDirChooser.class:1090
#, fuzzy
msgid "Show hidden files"
msgstr "显示隐藏文件(&h)"

#: FDirChooser.class:1098
#, fuzzy
msgid "Rename"
msgstr "重命名(&R)"

#: FDirChooser.class:1103
msgid "Copy"
msgstr "复制"

#: FDirChooser.class:1108
#, fuzzy
msgid "Delete"
msgstr "删除(&D)"

#: FDirChooser.class:1120
msgid "&Uncompress file"
msgstr "解压缩文件(&U)"

#: FDirChooser.class:1125
msgid "&Create directory"
msgstr "创建目录(&C)"

#: FDirChooser.class:1130
msgid "Open in &file manager..."
msgstr "用文件管理器打开...（&f）"

#: FDirChooser.class:1135
msgid "&Refresh"
msgstr "更新(&R)"

#: FDirChooser.class:1143
msgid "&Properties"
msgstr "属性(&P)"

#: FDirChooser.class:1333
msgid "Overwrite all"
msgstr "全部覆盖"

#: FDirChooser.class:1333
msgid "This file or directory already exists."
msgstr "该文件或目录已经存在。"

#: FDirChooser.class:1354
msgid "Cannot list archive contents"
msgstr "不能列出文档内容。"

#: FDirChooser.class:1394
msgid "Cannot uncompress file."
msgstr "不能解压缩文件。"

#: FDirChooser.class:1394
msgid "Unknown archive."
msgstr "未知文档。"

#: FDirChooser.class:1465
msgid "Delete file"
msgstr "删除文件"

#: FDirChooser.class:1466
msgid "&Delete"
msgstr "删除(&D)"

#: FDirChooser.class:1466
msgid "Do you really want to delete that file?"
msgstr "确定要删除文件吗？"

#: FDirChooser.class:1473
msgid "Unable to delete file."
msgstr "不能删除文件。"

#: FDirChooser.class:1483
msgid "Delete directory"
msgstr "删除目录"

#: FDirChooser.class:1484
msgid "Do you really want to delete that directory?"
msgstr "是否确实要删除该目录？"

#: FDirChooser.class:1491
msgid "Unable to delete directory."
msgstr "无法删除目录。"

#: FDirChooser.form:76
msgid "Parent directory"
msgstr "上级目录"

#: FDirChooser.form:82
msgid "Root directory"
msgstr "根目录"

#: FDirChooser.form:153
msgid "Icon view"
msgstr ""

#: FDirChooser.form:164
msgid "Compact view"
msgstr ""

#: FDirChooser.form:174
#, fuzzy
msgid "Preview view"
msgstr "预览"

#: FDirChooser.form:184
msgid "Detailed view"
msgstr "查看细节"

#: FDirChooser.form:197
msgid "File properties"
msgstr "文件属性"

#: FDirChooser.form:203
msgid "Show files"
msgstr "显示文件"

#: FDirChooser.form:223
msgid "Bookmarks"
msgstr "书签"

#: FDirChooser.form:290 FInputBox.form:43 FWizard.class:76 Form1.form:41
msgid "OK"
msgstr "确定"

#: FDirChooser.form:296 FEditBookmark.class:119 FInputBox.form:49
#: FSidePanel.class:1146 FWizard.form:52 Form1.form:47
msgid "Cancel"
msgstr "取消"

#: FDocumentView.form:58
msgid "Zoom :"
msgstr ""

#: FDocumentView.form:63
msgid "Show Shadow"
msgstr ""

#: FDocumentView.form:73 FTestTabPanel.form:72
msgid "Padding"
msgstr ""

#: FDocumentView.form:78
msgid "Spacing"
msgstr ""

#: FDocumentView.form:87
msgid "Scale Mode"
msgstr ""

#: FDocumentView.form:96
msgid "Goto :"
msgstr ""

#: FDocumentView.form:102
msgid "Column"
msgstr ""

#: FDocumentView.form:102
msgid "Fill"
msgstr ""

#: FDocumentView.form:102
msgid "Horizontal"
msgstr ""

#: FDocumentView.form:102
msgid "None"
msgstr ""

#: FDocumentView.form:102
msgid "Row"
msgstr ""

#: FDocumentView.form:102
msgid "Vertical"
msgstr ""

#: FDocumentView.form:103 FMain.form:118
msgid "ComboBox1"
msgstr ""

#: FDocumentView.form:108 FMain.form:103 FTestBalloon.form:19
#: FTestCompletion.form:23 FTestMenuButton.form:150 FTestMessageView.form:27
#: FTestWizard.form:24
msgid "Button1"
msgstr ""

#: FDocumentView.form:117
msgid "Columns"
msgstr ""

#: FDocumentView.form:127
msgid "Center"
msgstr ""

#: FEditBookmark.class:23 FileView.class:198
msgid "Name"
msgstr "名称"

#: FEditBookmark.class:24
msgid "Path"
msgstr "路径"

#: FEditBookmark.class:119
msgid "Do you really want to remove this bookmark?"
msgstr "确定要移除该书签吗?"

#: FEditBookmark.form:15
msgid "Edit bookmarks"
msgstr "编辑书签"

#: FEditBookmark.form:34
msgid "Up"
msgstr "上移"

#: FEditBookmark.form:40
msgid "Down"
msgstr "下移"

#: FEditBookmark.form:46 FListEditor.class:277
msgid "Remove"
msgstr "移除"

#: FEditBookmark.form:57 FFileProperties.form:292
msgid "Close"
msgstr "关闭"

#: FFileProperties.class:120
msgid "Image"
msgstr "图像"

#: FFileProperties.class:125
msgid "Audio"
msgstr "音频"

#: FFileProperties.class:129
msgid "Video"
msgstr "视频"

#: FFileProperties.class:189
msgid "&1 properties"
msgstr "&1属性"

# gb-ignore
#: FFileProperties.class:220 Main.module:379
msgid "&1 B"
msgstr ""

# gb-ignore
#: FFileProperties.class:225
msgid "no file"
msgstr ""

# gb-ignore
#: FFileProperties.class:227
msgid "one file"
msgstr ""

#: FFileProperties.class:229
msgid "files"
msgstr "文件"

# gb-ignore
#: FFileProperties.class:233
msgid "no directory"
msgstr ""

# gb-ignore
#: FFileProperties.class:235
msgid "one directory"
msgstr ""

#: FFileProperties.class:237
msgid "directories"
msgstr "目录"

#: FFileProperties.form:56
msgid "General"
msgstr "常规"

#: FFileProperties.form:86
msgid "Type"
msgstr "类型"

#: FFileProperties.form:99
msgid "Link"
msgstr "链接"

#: FFileProperties.form:114
msgid "Directory"
msgstr "目录"

#: FFileProperties.form:127 FileView.class:199
msgid "Size"
msgstr "大小"

#: FFileProperties.form:139 FileView.class:200
msgid "Last modified"
msgstr "最近修改"

#: FFileProperties.form:151
#, fuzzy
msgid "Last access"
msgstr "最近颜色"

#: FFileProperties.form:163 FileView.class:201
msgid "Permissions"
msgstr "许可权限"

#: FFileProperties.form:176
msgid "Owner"
msgstr "属主"

#: FFileProperties.form:188
msgid "Group"
msgstr "属组"

#: FFileProperties.form:198
msgid "Preview"
msgstr "预览"

#: FFileProperties.form:263
msgid "Errors"
msgstr "错误"

#: FFontChooser.class:374
msgid "How quickly daft jumping zebras vex"
msgstr "One CHINA, One family. 一个中国，一个家。"

#: FFontChooser.form:74
msgid "Building cache"
msgstr ""

#: FFontChooser.form:81
#, fuzzy
msgid "Refresh cache"
msgstr "更新(&R)"

#: FFontChooser.form:87
#, fuzzy
msgid "Show font preview"
msgstr "显示图像预览(&i)"

#: FFontChooser.form:111
msgid "Bold"
msgstr "粗体"

#: FFontChooser.form:118
msgid "Italic"
msgstr "斜体"

#: FFontChooser.form:125
msgid "Underline"
msgstr "下划线"

#: FFontChooser.form:132
msgid "Strikeout"
msgstr "删除线"

#: FFontChooser.form:141
msgid "Relative"
msgstr "相对尺寸"

#: FIconPanel.form:18
msgid "Tab 1"
msgstr ""

#: FIconPanel.form:22 FTestTabPanel.form:33
msgid "Toto"
msgstr ""

#: FLCDLabel.form:15
msgid "12:34"
msgstr ""

#: FListEditor.class:276
msgid "Add"
msgstr "添加"

#: FListEditor.form:45
msgid "Add item"
msgstr "添加条目"

#: FListEditor.form:53
msgid "Remove item"
msgstr "移除条目"

#: FListEditor.form:61
msgid "Move item up"
msgstr "上移条目"

#: FListEditor.form:69
msgid "Move item down"
msgstr "下移条目"

#: FMain.class:25
#, fuzzy
msgid "PDF files"
msgstr "文件"

#: FMain.class:25
msgid "Postscript files"
msgstr ""

#: FMain.form:31 FTestFileChooser.form:31 FTestMenuButton.form:40 FWiki.form:20
msgid "Menu2"
msgstr ""

#: FMain.form:35 FTestFileChooser.form:36 FTestMenuButton.form:44 FWiki.form:24
msgid "Menu3"
msgstr ""

#: FMain.form:84
msgid "http://gambas.sf.net"
msgstr ""

#: FMain.form:92
msgid "ComboBox2"
msgstr ""

#: FMain.form:98 FTestBalloon.form:13 FTestFileChooser.form:86
#: FTestSwitchButton.form:25
msgid "TextBox1"
msgstr ""

#: FMain.form:108 Form2.form:121
msgid "MenuButton1"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 1"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 2"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 3"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 4"
msgstr ""

#: FMessage.form:39
msgid "Do not display this message again"
msgstr "不再显示该信息"

#: FSidePanel.class:1125
msgid "Hidden"
msgstr "隐藏"

#: FSidePanel.class:1132
#, fuzzy
msgid "Transparent handle"
msgstr "透明"

#: FSidePanel.class:1140
msgid "Hide automatically"
msgstr ""

#: FSpinBar.form:24
msgid "Test"
msgstr ""

#: FTestBalloon.form:18
msgid "Ceci est une bulle d'aide"
msgstr ""

#: FTestColorChooser.form:20
msgid "Resizable"
msgstr ""

#: FTestCompletion.form:28 FTestMessageView.form:38
msgid "Button2"
msgstr ""

#: FTestDateChooser.form:33
msgid "Enable"
msgstr ""

#: FTestExpander.form:16
msgid "Expander"
msgstr ""

#: FTestFileBox.form:17
msgid "Open a file"
msgstr ""

#: FTestFileBox.form:18
msgid "File to open"
msgstr ""

#: FTestFileBox.form:24
#, fuzzy
msgid "Save file as"
msgstr "显示文件"

#: FTestFileBox.form:25
msgid "File to save"
msgstr ""

#: FTestFileBox.form:30
msgid "Open image file"
msgstr ""

#: FTestFileBox.form:33
#, fuzzy
msgid "Image filters"
msgstr "图像预览"

#: FTestFileChooser.form:28 FTestMenuButton.form:36
msgid "Menu1"
msgstr ""

#: FTestFileChooser.form:41 FTestMenuButton.form:70
msgid "Menu7"
msgstr ""

#: FTestFileChooser.form:49 FTestMenuButton.form:53 FWiki.form:28
msgid "Menu4"
msgstr ""

#: FTestFileChooser.form:54 FTestMenuButton.form:57 FWiki.form:32
msgid "Menu5"
msgstr ""

#: FTestFileChooser.form:76
msgid "Balloon"
msgstr ""

#: FTestFileChooser.form:81 FTestSwitchButton.form:42
msgid "Label1"
msgstr ""

#: FTestFileView.form:22
msgid "Selection"
msgstr ""

#: FTestListEditor.form:11
msgid "Élément 5"
msgstr ""

#: FTestMenuButton.form:32
msgid "Project"
msgstr ""

#: FTestMenuButton.form:49
msgid "View"
msgstr ""

#: FTestMenuButton.form:61
msgid "Menu6"
msgstr ""

#: FTestMenuButton.form:66
msgid "Tools"
msgstr ""

#: FTestMenuButton.form:74
msgid "Menu8"
msgstr ""

#: FTestMenuButton.form:78
msgid "Menu9"
msgstr ""

#: FTestMenuButton.form:81
msgid "Menu10"
msgstr ""

#: FTestMenuButton.form:85
msgid "Menu11"
msgstr ""

#: FTestMenuButton.form:124
msgid "Menu button"
msgstr ""

#: FTestSwitchButton.form:49
msgid "Label2"
msgstr ""

#: FTestTabPanel.form:43
msgid "Text"
msgstr ""

#: FTestTabPanel.form:49
msgid "Tab 4"
msgstr ""

#: FTestTabPanel.form:51
msgid "Tab 5"
msgstr ""

#: FTestTabPanel.form:53
msgid "Tab 6"
msgstr ""

#: FTestTabPanel.form:62 FTestTableView.form:20
msgid "Border"
msgstr ""

#: FTestTabPanel.form:67
msgid "Orientation"
msgstr ""

#: FTestTabPanel.form:77
msgid "TabBar"
msgstr ""

#: FTestToolPanel.form:17
msgid "Toolbar 1"
msgstr ""

#: FTestToolPanel.form:19
msgid "Toolbar 2"
msgstr ""

#: FTestToolPanel.form:21
msgid "Toolbar 3"
msgstr ""

#: FTestToolPanel.form:23
msgid "Toolbar 4"
msgstr ""

#: FTestValueBox.form:16
msgid "Hello world!"
msgstr ""

#: FTestWizard.form:20
msgid "Étape n°1"
msgstr ""

#: FTestWizard.form:27
msgid "Ceci est une longue étape"
msgstr ""

#: FTestWizard.form:33
msgid "Étape n°3"
msgstr ""

#: FTestWizard.form:35
msgid "Étape n°4"
msgstr ""

#: FWizard.class:88
msgid "&Next"
msgstr "下一个(&N)"

#: FWizard.form:58
msgid "Next"
msgstr "下一步"

#: FWizard.form:64
msgid "Previous"
msgstr "上一步"

#: FileView.class:222
msgid "This folder is empty."
msgstr ""

#: FileView.class:1398
msgid "Cannot rename file."
msgstr "不能重命名文件。"

#: Form2.form:126
msgid "ButtonBox2"
msgstr ""

#: Form3.form:25
msgid "Raise"
msgstr ""

#: Help.module:71
msgid "A file or directory name cannot be void."
msgstr "文件或目录名称不能无效。"

#: Help.module:72
msgid "The '/' character is forbidden inside file or directory names."
msgstr "文件或目录名称中禁止出现'/'字符。"

#: Main.module:381
msgid "&1 KiB"
msgstr "&1 KB"

#: Main.module:383
msgid "&1 MiB"
msgstr "&1 MB"

#: Main.module:385
msgid "&1 GiB"
msgstr "&1 GB"

#: Main.module:391
#, fuzzy
msgid "&1 KB"
msgstr "&1 KB"

#: Main.module:393
#, fuzzy
msgid "&1 MB"
msgstr "&1 MB"

#: Main.module:395
#, fuzzy
msgid "&1 GB"
msgstr "&1 GB"

#: Wizard.class:86
msgid "Step #&1"
msgstr "步骤#&1"

#~ msgid "&Overwrite"
#~ msgstr "(&O)覆盖"

#~ msgid "No file in this folder."
#~ msgstr "该目录中没有文件。"

#~ msgid "Show &details"
#~ msgstr "显示细节(&d)"
