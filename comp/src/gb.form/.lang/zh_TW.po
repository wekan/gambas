#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form 3.14.90\n"
"POT-Creation-Date: 2024-09-16 12:46 UTC\n"
"PO-Revision-Date: 2020-06-06 18:09 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "More controls for graphical components"
msgstr "更多圖形元件控制"

#: CBookmarkList.class:51
msgid "Home"
msgstr "家"

#: CBookmarkList.class:53
msgid "Desktop"
msgstr "桌面"

#: CBookmarkList.class:55
msgid "System"
msgstr "系統"

#: ColorPalette.class:185
msgid "Last colors"
msgstr ""

#: ColorPalette.class:317
msgid "Remove color"
msgstr ""

#: ColorPalette.class:321
msgid "Remove all colors"
msgstr ""

#: ColorPalette.class:325
msgid "Sort colors"
msgstr ""

#: DirView.class:597
msgid "Cannot rename directory."
msgstr ""

#: DirView.class:627
msgid "New folder"
msgstr "新增資料夾"

#: DirView.class:646
msgid "Cannot create directory."
msgstr ""

#: FBugFileView.form:32
msgid "Toggle Fileview Detailed View"
msgstr ""

#: FBugFileView.form:37
msgid "Toggle Filechooser Detailed View"
msgstr ""

#: FCalendar.form:48
msgid "Today"
msgstr "今天"

#: FCalendar.form:54
msgid "Previous month"
msgstr "上一個月"

#: FCalendar.form:60
msgid "Next month"
msgstr "下一個月"

#: FCalendar.form:148
msgid "Apply"
msgstr ""

#: FColorChooser.form:81
msgid "Follow color grid"
msgstr ""

#: FDirChooser.class:441
msgid "Directory not found."
msgstr ""

#: FDirChooser.class:550
msgid "All files (*)"
msgstr ""

#: FDirChooser.class:911
msgid "Overwrite"
msgstr "覆寫(O)"

#: FDirChooser.class:911
msgid ""
"This file already exists.\n"
"\n"
"Do you want to overwrite it?"
msgstr ""

#: FDirChooser.class:1029
msgid "&Bookmark current directory"
msgstr ""

#: FDirChooser.class:1037
msgid "&Edit bookmarks..."
msgstr ""

#: FDirChooser.class:1081
msgid "Preview big files"
msgstr ""

#: FDirChooser.class:1090
msgid "Show hidden files"
msgstr ""

#: FDirChooser.class:1098
msgid "Rename"
msgstr ""

#: FDirChooser.class:1103
msgid "Copy"
msgstr "複製"

#: FDirChooser.class:1108
msgid "Delete"
msgstr ""

#: FDirChooser.class:1120
msgid "&Uncompress file"
msgstr ""

#: FDirChooser.class:1125
msgid "&Create directory"
msgstr ""

#: FDirChooser.class:1130
msgid "Open in &file manager..."
msgstr ""

#: FDirChooser.class:1135
msgid "&Refresh"
msgstr ""

#: FDirChooser.class:1143
msgid "&Properties"
msgstr ""

#: FDirChooser.class:1333
msgid "Overwrite all"
msgstr ""

#: FDirChooser.class:1333
msgid "This file or directory already exists."
msgstr ""

#: FDirChooser.class:1354
msgid "Cannot list archive contents"
msgstr ""

#: FDirChooser.class:1394
msgid "Cannot uncompress file."
msgstr ""

#: FDirChooser.class:1394
msgid "Unknown archive."
msgstr ""

#: FDirChooser.class:1465
msgid "Delete file"
msgstr ""

#: FDirChooser.class:1466
msgid "&Delete"
msgstr ""

#: FDirChooser.class:1466
msgid "Do you really want to delete that file?"
msgstr ""

#: FDirChooser.class:1473
msgid "Unable to delete file."
msgstr ""

#: FDirChooser.class:1483
msgid "Delete directory"
msgstr ""

#: FDirChooser.class:1484
msgid "Do you really want to delete that directory?"
msgstr ""

#: FDirChooser.class:1491
msgid "Unable to delete directory."
msgstr ""

#: FDirChooser.form:76
msgid "Parent directory"
msgstr ""

#: FDirChooser.form:82
msgid "Root directory"
msgstr ""

#: FDirChooser.form:153
msgid "Icon view"
msgstr ""

#: FDirChooser.form:164
msgid "Compact view"
msgstr ""

#: FDirChooser.form:174
#, fuzzy
msgid "Preview view"
msgstr "預覽"

#: FDirChooser.form:184
msgid "Detailed view"
msgstr ""

#: FDirChooser.form:197
msgid "File properties"
msgstr ""

#: FDirChooser.form:203
msgid "Show files"
msgstr ""

#: FDirChooser.form:223
msgid "Bookmarks"
msgstr ""

#: FDirChooser.form:290 FInputBox.form:43 FWizard.class:76 Form1.form:41
msgid "OK"
msgstr "確定"

#: FDirChooser.form:296 FEditBookmark.class:119 FInputBox.form:49
#: FSidePanel.class:1146 FWizard.form:52 Form1.form:47
msgid "Cancel"
msgstr "取消"

#: FDocumentView.form:58
msgid "Zoom :"
msgstr ""

#: FDocumentView.form:63
msgid "Show Shadow"
msgstr ""

#: FDocumentView.form:73 FTestTabPanel.form:72
msgid "Padding"
msgstr ""

#: FDocumentView.form:78
msgid "Spacing"
msgstr ""

#: FDocumentView.form:87
msgid "Scale Mode"
msgstr ""

#: FDocumentView.form:96
msgid "Goto :"
msgstr ""

#: FDocumentView.form:102
msgid "Column"
msgstr ""

#: FDocumentView.form:102
msgid "Fill"
msgstr ""

#: FDocumentView.form:102
msgid "Horizontal"
msgstr ""

#: FDocumentView.form:102
msgid "None"
msgstr ""

#: FDocumentView.form:102
msgid "Row"
msgstr ""

#: FDocumentView.form:102
msgid "Vertical"
msgstr ""

#: FDocumentView.form:103 FMain.form:118
msgid "ComboBox1"
msgstr ""

#: FDocumentView.form:108 FMain.form:103 FTestBalloon.form:19
#: FTestCompletion.form:23 FTestMenuButton.form:150 FTestMessageView.form:27
#: FTestWizard.form:24
msgid "Button1"
msgstr ""

#: FDocumentView.form:117
msgid "Columns"
msgstr ""

#: FDocumentView.form:127
msgid "Center"
msgstr ""

#: FEditBookmark.class:23 FileView.class:198
msgid "Name"
msgstr "名稱"

#: FEditBookmark.class:24
msgid "Path"
msgstr "路徑"

#: FEditBookmark.class:119
msgid "Do you really want to remove this bookmark?"
msgstr ""

#: FEditBookmark.form:15
msgid "Edit bookmarks"
msgstr ""

#: FEditBookmark.form:34
msgid "Up"
msgstr "上"

#: FEditBookmark.form:40
msgid "Down"
msgstr "下"

#: FEditBookmark.form:46 FListEditor.class:277
msgid "Remove"
msgstr "移除"

#: FEditBookmark.form:57 FFileProperties.form:292
msgid "Close"
msgstr "關閉"

#: FFileProperties.class:120
msgid "Image"
msgstr "影像"

#: FFileProperties.class:125
msgid "Audio"
msgstr ""

#: FFileProperties.class:129
msgid "Video"
msgstr "視訊"

#: FFileProperties.class:189
msgid "&1 properties"
msgstr ""

#: FFileProperties.class:220 Main.module:379
msgid "&1 B"
msgstr ""

#: FFileProperties.class:225
msgid "no file"
msgstr ""

#: FFileProperties.class:227
msgid "one file"
msgstr ""

#: FFileProperties.class:229
msgid "files"
msgstr ""

#: FFileProperties.class:233
msgid "no directory"
msgstr ""

#: FFileProperties.class:235
msgid "one directory"
msgstr ""

#: FFileProperties.class:237
msgid "directories"
msgstr ""

#: FFileProperties.form:56
msgid "General"
msgstr "一般的"

#: FFileProperties.form:86
msgid "Type"
msgstr "類型"

#: FFileProperties.form:99
msgid "Link"
msgstr ""

#: FFileProperties.form:114
msgid "Directory"
msgstr "目錄"

#: FFileProperties.form:127 FileView.class:199
msgid "Size"
msgstr "大小"

#: FFileProperties.form:139 FileView.class:200
msgid "Last modified"
msgstr ""

#: FFileProperties.form:151
msgid "Last access"
msgstr ""

#: FFileProperties.form:163 FileView.class:201
msgid "Permissions"
msgstr ""

#: FFileProperties.form:176
msgid "Owner"
msgstr ""

#: FFileProperties.form:188
msgid "Group"
msgstr ""

#: FFileProperties.form:198
msgid "Preview"
msgstr "預覽"

#: FFileProperties.form:263
msgid "Errors"
msgstr "錯誤"

#: FFontChooser.class:374
msgid "How quickly daft jumping zebras vex"
msgstr ""

#: FFontChooser.form:74
msgid "Building cache"
msgstr ""

#: FFontChooser.form:81
msgid "Refresh cache"
msgstr ""

#: FFontChooser.form:87
msgid "Show font preview"
msgstr ""

#: FFontChooser.form:111
msgid "Bold"
msgstr "粗體"

#: FFontChooser.form:118
msgid "Italic"
msgstr "斜體"

#: FFontChooser.form:125
msgid "Underline"
msgstr "底線"

#: FFontChooser.form:132
msgid "Strikeout"
msgstr "刪除線"

#: FFontChooser.form:141
msgid "Relative"
msgstr ""

#: FIconPanel.form:18
msgid "Tab 1"
msgstr ""

#: FIconPanel.form:22 FTestTabPanel.form:33
msgid "Toto"
msgstr ""

#: FLCDLabel.form:15
msgid "12:34"
msgstr ""

#: FListEditor.class:276
msgid "Add"
msgstr "新增"

#: FListEditor.form:45
msgid "Add item"
msgstr ""

#: FListEditor.form:53
msgid "Remove item"
msgstr ""

#: FListEditor.form:61
msgid "Move item up"
msgstr ""

#: FListEditor.form:69
msgid "Move item down"
msgstr ""

#: FMain.class:25
msgid "PDF files"
msgstr ""

#: FMain.class:25
msgid "Postscript files"
msgstr ""

#: FMain.form:31 FTestFileChooser.form:31 FTestMenuButton.form:40 FWiki.form:20
msgid "Menu2"
msgstr ""

#: FMain.form:35 FTestFileChooser.form:36 FTestMenuButton.form:44 FWiki.form:24
msgid "Menu3"
msgstr ""

#: FMain.form:84
msgid "http://gambas.sf.net"
msgstr ""

#: FMain.form:92
msgid "ComboBox2"
msgstr ""

#: FMain.form:98 FTestBalloon.form:13 FTestFileChooser.form:86
#: FTestSwitchButton.form:25
msgid "TextBox1"
msgstr ""

#: FMain.form:108 Form2.form:121
msgid "MenuButton1"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 1"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 2"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 3"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 4"
msgstr ""

#: FMessage.form:39
msgid "Do not display this message again"
msgstr ""

#: FSidePanel.class:1125
msgid "Hidden"
msgstr ""

#: FSidePanel.class:1132
msgid "Transparent handle"
msgstr ""

#: FSidePanel.class:1140
msgid "Hide automatically"
msgstr ""

#: FSpinBar.form:24
msgid "Test"
msgstr ""

#: FTestBalloon.form:18
msgid "Ceci est une bulle d'aide"
msgstr ""

#: FTestColorChooser.form:20
msgid "Resizable"
msgstr ""

#: FTestCompletion.form:28 FTestMessageView.form:38
msgid "Button2"
msgstr ""

#: FTestDateChooser.form:33
msgid "Enable"
msgstr ""

#: FTestExpander.form:16
msgid "Expander"
msgstr ""

#: FTestFileBox.form:17
msgid "Open a file"
msgstr ""

#: FTestFileBox.form:18
msgid "File to open"
msgstr ""

#: FTestFileBox.form:24
msgid "Save file as"
msgstr ""

#: FTestFileBox.form:25
msgid "File to save"
msgstr ""

#: FTestFileBox.form:30
msgid "Open image file"
msgstr ""

#: FTestFileBox.form:33
msgid "Image filters"
msgstr ""

#: FTestFileChooser.form:28 FTestMenuButton.form:36
msgid "Menu1"
msgstr ""

#: FTestFileChooser.form:41 FTestMenuButton.form:70
msgid "Menu7"
msgstr ""

#: FTestFileChooser.form:49 FTestMenuButton.form:53 FWiki.form:28
msgid "Menu4"
msgstr ""

#: FTestFileChooser.form:54 FTestMenuButton.form:57 FWiki.form:32
msgid "Menu5"
msgstr ""

#: FTestFileChooser.form:76
msgid "Balloon"
msgstr ""

#: FTestFileChooser.form:81 FTestSwitchButton.form:42
msgid "Label1"
msgstr ""

#: FTestFileView.form:22
msgid "Selection"
msgstr ""

#: FTestListEditor.form:11
msgid "Élément 5"
msgstr ""

#: FTestMenuButton.form:32
msgid "Project"
msgstr ""

#: FTestMenuButton.form:49
msgid "View"
msgstr ""

#: FTestMenuButton.form:61
msgid "Menu6"
msgstr ""

#: FTestMenuButton.form:66
msgid "Tools"
msgstr ""

#: FTestMenuButton.form:74
msgid "Menu8"
msgstr ""

#: FTestMenuButton.form:78
msgid "Menu9"
msgstr ""

#: FTestMenuButton.form:81
msgid "Menu10"
msgstr ""

#: FTestMenuButton.form:85
msgid "Menu11"
msgstr ""

#: FTestMenuButton.form:124
msgid "Menu button"
msgstr ""

#: FTestSwitchButton.form:49
msgid "Label2"
msgstr ""

#: FTestTabPanel.form:43
msgid "Text"
msgstr ""

#: FTestTabPanel.form:49
msgid "Tab 4"
msgstr ""

#: FTestTabPanel.form:51
msgid "Tab 5"
msgstr ""

#: FTestTabPanel.form:53
msgid "Tab 6"
msgstr ""

#: FTestTabPanel.form:62 FTestTableView.form:20
msgid "Border"
msgstr ""

#: FTestTabPanel.form:67
msgid "Orientation"
msgstr ""

#: FTestTabPanel.form:77
msgid "TabBar"
msgstr ""

#: FTestToolPanel.form:17
msgid "Toolbar 1"
msgstr ""

#: FTestToolPanel.form:19
msgid "Toolbar 2"
msgstr ""

#: FTestToolPanel.form:21
msgid "Toolbar 3"
msgstr ""

#: FTestToolPanel.form:23
msgid "Toolbar 4"
msgstr ""

#: FTestValueBox.form:16
msgid "Hello world!"
msgstr ""

#: FTestWizard.form:20
msgid "Étape n°1"
msgstr ""

#: FTestWizard.form:27
msgid "Ceci est une longue étape"
msgstr ""

#: FTestWizard.form:33
msgid "Étape n°3"
msgstr ""

#: FTestWizard.form:35
msgid "Étape n°4"
msgstr ""

#: FWizard.class:88
msgid "&Next"
msgstr "下一頁(&N)"

#: FWizard.form:58
msgid "Next"
msgstr "下一個(N)"

#: FWizard.form:64
msgid "Previous"
msgstr "上一頁(P)"

#: FileView.class:222
msgid "This folder is empty."
msgstr ""

#: FileView.class:1398
msgid "Cannot rename file."
msgstr ""

#: Form2.form:126
msgid "ButtonBox2"
msgstr ""

#: Form3.form:25
msgid "Raise"
msgstr ""

#: Help.module:71
msgid "A file or directory name cannot be void."
msgstr ""

#: Help.module:72
msgid "The '/' character is forbidden inside file or directory names."
msgstr ""

#: Main.module:381
msgid "&1 KiB"
msgstr ""

#: Main.module:383
msgid "&1 MiB"
msgstr ""

#: Main.module:385
msgid "&1 GiB"
msgstr ""

#: Main.module:391
msgid "&1 KB"
msgstr ""

#: Main.module:393
msgid "&1 MB"
msgstr ""

#: Main.module:395
msgid "&1 GB"
msgstr ""

#: Wizard.class:86
msgid "Step #&1"
msgstr ""
