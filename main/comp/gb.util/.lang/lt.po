#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:31 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

#: File.class:22
msgid "&1 B"
msgstr ""

#: File.class:24
msgid "&1 KiB"
msgstr ""

#: File.class:26
msgid "&1 MiB"
msgstr ""

#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr ""

# gb-ignore
#: Language.class:17
msgid "Arabic (Egypt)"
msgstr ""

# gb-ignore
#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr ""

# gb-ignore
#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr ""

# gb-ignore
#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr ""

# gb-ignore
#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr ""

# gb-ignore
#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr ""

# gb-ignore
#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr ""

# gb-ignore
#: Language.class:37
msgid "Danish (Denmark)"
msgstr ""

# gb-ignore
#: Language.class:40
msgid "German (Germany)"
msgstr ""

# gb-ignore
#: Language.class:41
msgid "German (Belgium)"
msgstr ""

# gb-ignore
#: Language.class:44
msgid "Greek (Greece)"
msgstr ""

# gb-ignore
#: Language.class:47
msgid "English (common)"
msgstr ""

# gb-ignore
#: Language.class:48
msgid "English (United Kingdom)"
msgstr ""

# gb-ignore
#: Language.class:49
msgid "English (U.S.A.)"
msgstr ""

# gb-ignore
#: Language.class:50
msgid "English (Australia)"
msgstr ""

# gb-ignore
#: Language.class:51
msgid "English (Canada)"
msgstr ""

# gb-ignore
#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr ""

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

# gb-ignore
#: Language.class:58
msgid "Spanish (Spain)"
msgstr ""

# gb-ignore
#: Language.class:59
msgid "Spanish (Argentina)"
msgstr ""

# gb-ignore
#: Language.class:62
msgid "Estonian (Estonia)"
msgstr ""

#: Language.class:65
msgid "Basque (Basque country)"
msgstr "Baskų (Baskų kraštas)"

# gb-ignore
#: Language.class:68
msgid "Farsi (Iran)"
msgstr ""

# gb-ignore
#: Language.class:71
msgid "Finnish (Finland)"
msgstr ""

# gb-ignore
#: Language.class:74
msgid "French (France)"
msgstr ""

# gb-ignore
#: Language.class:75
msgid "French (Belgium)"
msgstr ""

# gb-ignore
#: Language.class:76
msgid "French (Canada)"
msgstr ""

# gb-ignore
#: Language.class:77
msgid "French (Switzerland)"
msgstr ""

# gb-ignore
#: Language.class:80
msgid "Galician (Spain)"
msgstr ""

# gb-ignore
#: Language.class:83
msgid "Hebrew (Israel)"
msgstr ""

# gb-ignore
#: Language.class:86
msgid "Hindi (India)"
msgstr ""

# gb-ignore
#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr ""

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr ""

# gb-ignore
#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr ""

# gb-ignore
#: Language.class:98
msgid "Irish (Ireland)"
msgstr ""

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr ""

# gb-ignore
#: Language.class:104
msgid "Italian (Italy)"
msgstr ""

# gb-ignore
#: Language.class:107
msgid "Japanese (Japan)"
msgstr ""

# gb-ignore
#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr ""

# gb-ignore
#: Language.class:113
msgid "Korean (Korea)"
msgstr ""

#: Language.class:116
msgid "Latin"
msgstr ""

# gb-ignore
#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr ""

# gb-ignore
#: Language.class:122
msgid "Malayalam (India)"
msgstr ""

# gb-ignore
#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr ""

# gb-ignore
#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr ""

# gb-ignore
#: Language.class:129
msgid "Dutch (Belgium)"
msgstr ""

# gb-ignore
#: Language.class:132
msgid "Norwegian (Norway)"
msgstr ""

# gb-ignore
#: Language.class:135
msgid "Punjabi (India)"
msgstr ""

# gb-ignore
#: Language.class:138
msgid "Polish (Poland)"
msgstr ""

# gb-ignore
#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr ""

# gb-ignore
#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr ""

# gb-ignore
#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr ""

# gb-ignore
#: Language.class:148
msgid "Romanian (Romania)"
msgstr ""

# gb-ignore
#: Language.class:151
msgid "Russian (Russia)"
msgstr ""

# gb-ignore
#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr ""

#: Language.class:157
msgid "Albanian (Albania)"
msgstr "Albanian (Albanų)"

# gb-ignore
#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr ""

# gb-ignore
#: Language.class:163
msgid "Swedish (Sweden)"
msgstr ""

# gb-ignore
#: Language.class:166
msgid "Turkish (Turkey)"
msgstr ""

# gb-ignore
#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr ""

# gb-ignore
#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr ""

# gb-ignore
#: Language.class:175
msgid "Wallon (Belgium)"
msgstr ""

# gb-ignore
#: Language.class:178
msgid "Simplified chinese (China)"
msgstr ""

# gb-ignore
#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr ""

#: Language.class:241
msgid "Unknown"
msgstr "Nežinomas"
