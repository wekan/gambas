Source: gambas3
Section: devel
Priority: optional
Homepage: @{website-url}@
Vcs-Browser: @{git-url}@
Maintainer: Willy Raets <gbWilly@proton.me>
Build-Depends:  automake,
                autoconf,
                bzip2,
                debhelper-compat (=12),
                dh-autoreconf,
                default-libmysqlclient-dev,
                gcc-11 [armhf], g++-11 [armhf],
                gettext,
                git,
                libalure-dev,
                libasound2-dev,
                libbz2-dev,
                libcairo2-dev,
                libcurl4-gnutls-dev,
                libdirectfb-dev,
                libdumb1-dev,
                libffi-dev,
                libfribidi-dev,
                libgdk-pixbuf2.0-dev,
                libglew-dev,
                libglib2.0-dev,
                libgmime-3.0-dev,
                libgmp-dev,
                libgsl-dev,
                libgstreamer-plugins-base1.0-dev,
                libgstreamer1.0-dev,
                libgtk2.0-dev,
                libgtk-3-dev,
                libgtkglext1-dev,
                libimlib2-dev,
                libmongoc-dev,
                libncurses5-dev,
                libpcre3-dev,
                libpoppler-dev,
                libpoppler-cpp-dev,
                libpoppler-glib-dev,
                libpoppler-private-dev,
                libpq-dev,
                libqt5opengl5-dev,
                libqt5svg5-dev,
                libqt5webkit5-dev,
                libqt5x11extras5-dev,
                librsvg2-dev,
                libsdl-image1.2-dev,
                libsdl-mixer1.2-dev,
                libsdl-sound1.2-dev,
                libsdl-ttf2.0-dev,
                libsdl2-dev,
                libsdl2-image-dev,
                libsdl2-mixer-dev,
                libsdl2-ttf-dev,
                libsqlite3-dev,
                libssl-dev,
                libtool,
                libv4l-dev,
                libwebkit2gtk-4.0-dev,
                libxml2-dev,
                libxslt1-dev,
                libxtst-dev,
                libzstd-dev,
                linux-libc-dev,
                pkg-config,
                qtbase5-dev,
                qtwebengine5-dev [amd64 arm64 armhf i386],
                sane-utils,
                unixodbc-dev,
                xdg-utils
Standards-Version: 4.5.0

Package: gambas3
Architecture: all
Section: devel
Depends: ${misc:Depends},
 gambas3-dev-tools (>= ${binary:Version}),
 gambas3-scripter (>= ${binary:Version}),
 gambas3-gb-args (>= ${binary:Version}),
 gambas3-gb-cairo (>= ${binary:Version}),
 gambas3-gb-chart (>= ${binary:Version}),
 gambas3-gb-clipper (>= ${binary:Version}),
 gambas3-gb-complex (>= ${binary:Version}),
 gambas3-gb-compress (>= ${binary:Version}),
 gambas3-gb-compress-bzlib2 (>= ${binary:Version}),
 gambas3-gb-compress-zlib (>= ${binary:Version}),
 gambas3-gb-compress-zstd (>= ${binary:Version}),
 gambas3-gb-crypt (>= ${binary:Version}),
 gambas3-gb-data (>= ${binary:Version}),
 gambas3-gb-db (>= ${binary:Version}),
 gambas3-gb-db-form (>= ${binary:Version}),
 gambas3-gb-db-mysql (>= ${binary:Version}),
 gambas3-gb-db-odbc (>= ${binary:Version}),
 gambas3-gb-db-postgresql (>= ${binary:Version}),
 gambas3-gb-db-sqlite3 (>= ${binary:Version}),
 gambas3-gb-dbus (>= ${binary:Version}),
 gambas3-gb-dbus-trayicon (>= ${binary:Version}),
 gambas3-gb-desktop (>= ${binary:Version}),
 gambas3-gb-desktop-x11 (>= ${binary:Version}),
 gambas3-gb-eval-highlight (>= ${binary:Version}),
 gambas3-gb-form (>= ${binary:Version}),
 gambas3-gb-form-dialog (>= ${binary:Version}),
 gambas3-gb-form-editor (>= ${binary:Version}),
 gambas3-gb-form-htmlview (>= ${binary:Version}),
 gambas3-gb-form-mdi (>= ${binary:Version}),
 gambas3-gb-form-print (>= ${binary:Version}),
 gambas3-gb-form-stock (>= ${binary:Version}),
 gambas3-gb-form-terminal (>= ${binary:Version}),
 gambas3-gb-gmp (>= ${binary:Version}),
 gambas3-gb-gsl (>= ${binary:Version}),
 gambas3-gb-gtk (>= ${binary:Version}),
 gambas3-gb-gtk-opengl (>= ${binary:Version}),
 gambas3-gb-gtk3 (>= ${binary:Version}),
 gambas3-gb-gtk3-opengl (>= ${binary:Version}),
 gambas3-gb-gtk3-wayland (>= ${binary:Version}),
 gambas3-gb-gtk3-webview (>= ${binary:Version}),
 gambas3-gb-gtk3-x11 (>= ${binary:Version}),
 gambas3-gb-highlight (>= ${binary:Version}),
 gambas3-gb-httpd (>= ${binary:Version}),
 gambas3-gb-image (>= ${binary:Version}),
 gambas3-gb-image-effect (>= ${binary:Version}),
 gambas3-gb-image-imlib (>= ${binary:Version}),
 gambas3-gb-image-io (>= ${binary:Version}),
 gambas3-gb-inotify (>= ${binary:Version}),
 gambas3-gb-libxml (>= ${binary:Version}),
 gambas3-gb-logging (>= ${binary:Version}),
 gambas3-gb-map (>= ${binary:Version}),
 gambas3-gb-markdown (>= ${binary:Version}),
 gambas3-gb-media (>= ${binary:Version}),
 gambas3-gb-media-form (>= ${binary:Version}),
 gambas3-gb-memcached (>= ${binary:Version}),
 gambas3-gb-mime (>= ${binary:Version}),
 gambas3-gb-mongodb (>= ${binary:Version}),
 gambas3-gb-mysql (>= ${binary:Version}),
 gambas3-gb-ncurses (>= ${binary:Version}),
 gambas3-gb-net (>= ${binary:Version}),
 gambas3-gb-net-curl (>= ${binary:Version}),
 gambas3-gb-net-pop3 (>= ${binary:Version}),
 gambas3-gb-net-smtp (>= ${binary:Version}),
 gambas3-gb-openal (>= ${binary:Version}),
 gambas3-gb-opengl (>= ${binary:Version}),
 gambas3-gb-opengl-glsl (>= ${binary:Version}),
 gambas3-gb-opengl-glu (>= ${binary:Version}),
 gambas3-gb-opengl-sge (>= ${binary:Version}),
 gambas3-gb-openssl (>= ${binary:Version}),
 gambas3-gb-option (>= ${binary:Version}),
 gambas3-gb-pcre (>= ${binary:Version}),
 gambas3-gb-pdf (>= ${binary:Version}),
 gambas3-gb-poppler (>= ${binary:Version}),
 gambas3-gb-qt5 (>= ${binary:Version}),
 gambas3-gb-qt5-ext (>= ${binary:Version}),
 gambas3-gb-qt5-opengl (>= ${binary:Version}),
 gambas3-gb-qt5-wayland (>= ${binary:Version}),
 gambas3-gb-qt5-webkit (>= ${binary:Version}),
 gambas3-gb-qt5-webview (>= ${binary:Version}),
 gambas3-gb-qt5-x11 (>= ${binary:Version}),
 gambas3-gb-report (>= ${binary:Version}),
 gambas3-gb-report2 (>= ${binary:Version}),
 gambas3-gb-scanner (>= ${binary:Version}),
 gambas3-gb-sdl (>= ${binary:Version}),
 gambas3-gb-sdl-sound (>= ${binary:Version}),
 gambas3-gb-sdl2 (>= ${binary:Version}),
 gambas3-gb-sdl2-audio (>= ${binary:Version}),
 gambas3-gb-settings (>= ${binary:Version}),
 gambas3-gb-signal (>= ${binary:Version}),
 gambas3-gb-term (>= ${binary:Version}),
 gambas3-gb-term-form (>= ${binary:Version}),
 gambas3-gb-util (>= ${binary:Version}),
 gambas3-gb-util-web (>= ${binary:Version}),
 gambas3-gb-v4l (>= ${binary:Version}),
 gambas3-gb-vb (>= ${binary:Version}),
 gambas3-gb-web (>= ${binary:Version}),
 gambas3-gb-web-feed (>= ${binary:Version}),
 gambas3-gb-web-form (>= ${binary:Version}),
 gambas3-gb-web-gui (>= ${binary:Version}),
 gambas3-gb-xml (>= ${binary:Version}),
 gambas3-gb-xml-html (>= ${binary:Version}),
 gambas3-gb-xml-rpc (>= ${binary:Version}),
 gambas3-gb-xml-xslt (>= ${binary:Version}),
 gambas3-gui (>= ${binary:Version}),
 gambas3-ide (>= ${binary:Version}),
 gambas3-runtime (>= ${binary:Version})
Description: Complete visual development environment for Gambas
 Gambas is a free development environment based on a Basic interpreter
 with object extensions, like Visual Basic(tm) (but it is NOT a clone!).
 With Gambas, you can quickly design your program GUI, access MySQL or
 PostgreSQL databases, pilot KDE applications with DCOP, translate your
 program into many languages, and so on...
 .
 This package doesn't include anything: it is a metapackage that installs the
 IDE and all the available Gambas components and tools.

Package: gambas3-dev
Architecture: all
Section: devel
Depends: gambas3-dev-tools,
         ${misc:Depends}
@{desc dev}@

Package: gambas3-devel
Architecture: all
Section: devel
Depends: gambas3-dev-tools,
         ${misc:Depends}
@{desc devel}@

Package: gambas3-dev-tools
Architecture: any
Section: devel
Depends: ${misc:Depends},
         ${shlibs:Depends},
         gambas3-runtime (>= ${binary:Version})
Breaks: gambas3-dev (<< 3.18.3), gambas3-devel (<< 3.18.3)
Replaces: gambas3-dev, gambas3-devel
Conflicts: gambas3-dev (<< 3.18.3), gambas3-devel (<< 3.18.3)
@{desc dev-tools}@

Package: gambas3-gui
Architecture: all
Section: libdevel
Depends: gambas3-gb-gtk (>= ${binary:Version}) | gambas3-gb-gtk3 (>= ${binary:Version}) | gambas3-gb-qt5 (>= ${binary:Version})
@{desc gui}@

Package: gambas3-ide
Architecture: all
Section: devel
Depends: ${misc:Depends},
         debhelper,
         fakeroot,
         gambas3-runtime (>= ${binary:Version}),
         gettext,
         rpm,
         tar,
         wget,
         @{dependencies ide}@
@{desc ide}@

Package: gambas3-runtime
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         ${shlibs:Depends}
Conflicts: gambas3-gb-gui,
           gambas3-gb-gui-opengl,
           gambas3-gb-jit
@{desc runtime}@

Package: gambas3-scripter
Architecture: all
Section: libdevel
Depends: ${misc:Depends},
         gambas3-dev-tools (>= ${binary:Version}),
         gambas3-gb-pcre (>= ${binary:Version}),
         gambas3-runtime (>= ${binary:Version})
Breaks: gambas3-script (<< 3.15.0)
Replaces: gambas3-script
Conflicts: gambas3-script (<< 3.15.0)
@{desc scripter}@

@{package gb.desktop
Depends: xdg-utils
}@

@{package gb.scanner
Depends: sane-utils
}@

@{packages}@
