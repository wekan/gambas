Format: 3.0 (quilt)
Source: gambas3
Binary: @{dsc-binary}@
Architecture: any all
Version: @{dsc-version}@
Maintainer: Debian Gambas Team <team+debian-gambas@tracker.debian.org>
Uploaders: José L. Redrejo Rodríguez <jredrejo@debian.org>
Homepage: @{website-url}@
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/gambas-team/gambas3
Vcs-Git: https://salsa.debian.org/gambas-team/gambas3.git
Build-Depends: debhelper-compat (= 12), gettext, libalure-dev, libbz2-dev, libcrypt-dev, libcurl4-gnutls-dev | libcurl-ssl-dev, libdbus-1-dev, libffi-dev, libglew-dev, libgmime-3.0-dev, libgmp-dev, libgsl-dev, libgstreamer-plugins-base1.0-dev, libgstreamer1.0-dev, libgtk-3-dev, libwebkit2gtk-4.0-dev [linux-any], libimlib2-dev, libjpeg-dev, libltdl-dev, default-libmysqlclient-dev, libncurses-dev, libopenal-dev, libpcre2-dev, libpng-dev, libpoppler-cpp-dev, libpoppler-glib-dev, libpoppler-private-dev, libpq-dev, libqt5svg5-dev, libqt5webkit5-dev, libqt5x11extras5-dev, librsvg2-dev, libsdl-ttf2.0-dev, libsdl2-dev, libsdl2-image-dev, libsdl2-mixer-dev, libsdl2-ttf-dev, libsqlite3-dev, libssl-dev, libv4l-dev [linux-any], libxml2-dev, libxslt1-dev, libxt-dev, libxtst-dev, libzstd-dev, linux-libc-dev [linux-any], mesa-common-dev, pkgconf, qtwebengine5-dev [amd64 arm64 armhf i386 mips64el], unixodbc-dev, xdg-utils, zlib1g-dev
Package-List:
@{dsc-package-list}@
DEBTRANSFORM-TAR: gambas-@VERSION@.tar.gz
DEBTRANSFORM-FILES-TAR: debian-Debian_11.tar.gz
