#
# spec file for package gambas3
#
# copyright (c) 2024 munix9@googlemail.com
#
# @{about}@
#

@{disable gb.db.sqlite2 gb.qt4* gb.qt5.wayland gb.qt5.x11 gb.gtk3.wayland gb.gtk3.x11 gb.qt6.wayland gb.qt6.x11 gb.qt5.ext gb.qt6.ext}@

%bcond_with     gb_mongodb
%if 0%{?suse_version} < 1600
%bcond_without  gb_qt5_webkit
%else
%bcond_with     gb_qt5_webkit
%endif

Name:           gambas3
Version:        @{package-version}@
Release:        0
Summary:        Complete visual development environment for Gambas
License:        GPL-2.0-or-later
URL:            @{website-url}@
Source0:        gambas-%{version}.tar.gz
Source99:       %{name}-openSUSE_Tumbleweed.rpmlintrc
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  git
BuildRequires:  hicolor-icon-theme
BuildRequires:  libtool
BuildRequires:  make
BuildRequires:  pkgconfig
BuildRequires:  xdg-utils
BuildRequires:  pkgconfig(libffi)
Requires:       %{name}-dev-tools = %{version}
Requires:       %{name}-gb-args = %{version}
Requires:       %{name}-gb-cairo = %{version}
Requires:       %{name}-gb-chart = %{version}
Requires:       %{name}-gb-clipper = %{version}
Requires:       %{name}-gb-complex = %{version}
Requires:       %{name}-gb-compress = %{version}
Requires:       %{name}-gb-compress-bzlib2 = %{version}
Requires:       %{name}-gb-compress-zlib = %{version}
Requires:       %{name}-gb-compress-zstd = %{version}
Requires:       %{name}-gb-crypt = %{version}
Requires:       %{name}-gb-data = %{version}
Requires:       %{name}-gb-db2 = %{version}
Requires:       %{name}-gb-db2-form = %{version}
Requires:       %{name}-gb-db2-mysql = %{version}
Requires:       %{name}-gb-db2-odbc = %{version}
Requires:       %{name}-gb-db2-postgresql = %{version}
Requires:       %{name}-gb-db2-sqlite3 = %{version}
Requires:       %{name}-gb-dbus = %{version}
Requires:       %{name}-gb-dbus-trayicon = %{version}
Requires:       %{name}-gb-desktop = %{version}
Requires:       %{name}-gb-desktop-x11 = %{version}
Requires:       %{name}-gb-form = %{version}
Requires:       %{name}-gb-form-dialog = %{version}
Requires:       %{name}-gb-form-editor = %{version}
Requires:       %{name}-gb-form-htmlview = %{version}
Requires:       %{name}-gb-form-mdi = %{version}
Requires:       %{name}-gb-form-print = %{version}
Requires:       %{name}-gb-form-stock = %{version}
Requires:       %{name}-gb-form-terminal = %{version}
Requires:       %{name}-gb-gmp = %{version}
Requires:       %{name}-gb-gsl = %{version}
Requires:       %{name}-gb-gtk = %{version}
Requires:       %{name}-gb-gtk-opengl = %{version}
Requires:       %{name}-gb-gtk3 = %{version}
Requires:       %{name}-gb-gtk3-opengl = %{version}
Requires:       %{name}-gb-gtk3-webview = %{version}
Requires:       %{name}-gb-highlight = %{version}
Requires:       %{name}-gb-httpd = %{version}
Requires:       %{name}-gb-image = %{version}
Requires:       %{name}-gb-image-effect = %{version}
Requires:       %{name}-gb-image-imlib = %{version}
Requires:       %{name}-gb-image-io = %{version}
Requires:       %{name}-gb-inotify = %{version}
Requires:       %{name}-gb-logging = %{version}
Requires:       %{name}-gb-map = %{version}
Requires:       %{name}-gb-markdown = %{version}
Requires:       %{name}-gb-media = %{version}
Requires:       %{name}-gb-media-form = %{version}
Requires:       %{name}-gb-memcached = %{version}
Requires:       %{name}-gb-mime = %{version}
Requires:       %{name}-gb-mysql = %{version}
Requires:       %{name}-gb-ncurses = %{version}
Requires:       %{name}-gb-net = %{version}
Requires:       %{name}-gb-net-curl = %{version}
Requires:       %{name}-gb-net-pop3 = %{version}
Requires:       %{name}-gb-net-smtp = %{version}
Requires:       %{name}-gb-openal = %{version}
Requires:       %{name}-gb-opengl = %{version}
Requires:       %{name}-gb-opengl-glsl = %{version}
Requires:       %{name}-gb-opengl-glu = %{version}
Requires:       %{name}-gb-opengl-sge = %{version}
Requires:       %{name}-gb-openssl = %{version}
Requires:       %{name}-gb-pcre = %{version}
Requires:       %{name}-gb-poppler = %{version}
Requires:       %{name}-gb-qt5 = %{version}
Requires:       %{name}-gb-qt5-opengl = %{version}
Requires:       %{name}-gb-qt5-webview = %{version}
Requires:       %{name}-gb-qt6 = %{version}
Requires:       %{name}-gb-qt6-opengl = %{version}
Requires:       %{name}-gb-qt6-webview = %{version}
Requires:       %{name}-gb-report = %{version}
Requires:       %{name}-gb-scanner = %{version}
Requires:       %{name}-gb-sdl2 = %{version}
Requires:       %{name}-gb-sdl2-audio = %{version}
Requires:       %{name}-gb-settings = %{version}
Requires:       %{name}-gb-signal = %{version}
Requires:       %{name}-gb-term = %{version}
Requires:       %{name}-gb-util = %{version}
Requires:       %{name}-gb-util-web = %{version}
Requires:       %{name}-gb-vb = %{version}
Requires:       %{name}-gb-web = %{version}
Requires:       %{name}-gb-web-feed = %{version}
Requires:       %{name}-gb-web-gui = %{version}
Requires:       %{name}-gb-xml = %{version}
Requires:       %{name}-gb-xml-html = %{version}
Requires:       %{name}-gb-xml-rpc = %{version}
Requires:       %{name}-gb-xml-xslt = %{version}
Requires:       %{name}-ide = %{version}
Requires:       %{name}-runtime = %{version}
Requires:       %{name}-scripter = %{version}
%if 0%{?suse_version} < 1600
BuildRequires:  gcc11-PIE
BuildRequires:  gcc11-c++
%else
BuildRequires:  gcc-c++
%endif

%description
Gambas is a free development environment based on a Basic interpreter
with object extensions, like Visual Basic(tm) (but it is NOT a clone!).

With Gambas, you can quickly design your program GUI, access MySQL or
PostgreSQL databases, pilot KDE applications with DCOP, translate your
program into many languages, and so on.

This package doesn't include anything: it is a metapackage to install
the IDE and optionally all the available Gambas components.

%package ide
Summary:        @{summary ide}@
License:        GPL-2.0-or-later AND OFL-1.1 AND MIT
@{dependencies ide}@
Requires:       (%{name}-gb-qt5-webview = %{version} or %{name}-gb-qt6-webview = %{version} or %{name}-gb-gtk3-webview = %{version})
Requires:       autoconf
Requires:       automake
Requires:       gettext-tools
Requires:       gzip
Requires:       patch
Requires:       rpm-build
Requires:       tar
Requires:       translate-shell
Requires:       unzip
Requires:       zip
Recommends:     pngquant
BuildArch:      noarch

%description ide
@{desc ide}@

%package runtime
Summary:        @{summary runtime}@
Recommends:     (gcc or clang)
Provides:       %{name}-gb-geom = %{version}
Obsoletes:      %{name}-gb-geom < %{version}
Provides:       %{name}-gb-gui = %{version}
Obsoletes:      %{name}-gb-gui < %{version}
Provides:       %{name}-gb-hash = %{version}
Obsoletes:      %{name}-gb-hash < %{version}
Provides:       %{name}-gb-jit = %{version}
Obsoletes:      %{name}-gb-jit < %{version}
Provides:       %{name}-gb-test = %{version}
Obsoletes:      %{name}-gb-test < %{version}

%description runtime
@{desc runtime}@

%package scripter
Summary:        @{summary scripter}@
Requires:       %{name}-dev-tools = %{version}
Requires:       %{name}-gb-pcre = %{version}
Requires:       %{name}-runtime = %{version}
Provides:       %{name}-script = %{version}
Obsoletes:      %{name}-script < %{version}
BuildArch:      noarch

%description scripter
@{desc scripter}@

%package dev-tools
Summary:        @{summary dev-tools}@
Requires:       %{name}-runtime = %{version}
Requires:       gettext-runtime
Provides:       %{name}-devel = %{version}
Obsoletes:      %{name}-devel < %{version}

%description dev-tools
@{desc dev-tools}@

%package benchmark
Summary:        Gambas/Perl/Python/Java benchmarks
Requires:       %{name}-gb-args = %{version}
Requires:       %{name}-scripter = %{version}
Requires:       java-headless >= 11
BuildArch:      noarch

%description benchmark
Gambas is a free development environment based on a Basic interpreter
with object extensions, like Visual Basic(tm) (but it is NOT a clone!).

Gambas/Perl/Python/Java scripts to compare benchmark.

%package examples
Summary:        Example projects provided with Gambas
Requires:       %{name}-ide = %{version}
BuildArch:      noarch

%description examples
Gambas is a free development environment based on a Basic interpreter
with object extensions, like Visual Basic(tm) (but it is NOT a clone!).

This package includes example projects provided with Gambas.

@{package gb.cairo
BuildRequires:  pkgconfig(cairo)
Requires:       %{name}-runtime = %{version}
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.clipper
License:        GPL-2.0-or-later AND BSL-1.0
Requires:       %{name}-runtime = %{version}
}@

@{package gb.compress.bzlib2
BuildRequires:  pkgconfig(bzip2)
Requires:       %{name}-gb-compress = %{version}
}@

@{package gb.compress.zlib
BuildRequires:  pkgconfig(zlib)
Requires:       %{name}-gb-compress = %{version}
}@

@{package gb.compress.zstd
BuildRequires:  pkgconfig(libzstd)
Requires:       %{name}-gb-compress = %{version}
}@

@{package gb.db.mysql
BuildRequires:  pkgconfig(libmariadb)
Requires:       %{name}-gb-db = %{version}
}@

@{package gb.db.odbc
BuildRequires:  pkgconfig(odbc)
Requires:       %{name}-gb-db = %{version}
}@

@{package gb.db.postgresql
BuildRequires:  postgresql-devel
Requires:       %{name}-gb-db = %{version}
}@

@{package gb.db.sqlite3
BuildRequires:  pkgconfig(sqlite3)
Requires:       %{name}-gb-db = %{version}
}@

@{package gb.db2.mysql
BuildRequires:  pkgconfig(libmariadb)
Requires:       %{name}-gb-db2 = %{version}
}@

@{package gb.db2.odbc
BuildRequires:  pkgconfig(odbc)
Requires:       %{name}-gb-db2 = %{version}
}@

@{package gb.db2.postgresql
BuildRequires:  postgresql-devel
Requires:       %{name}-gb-db2 = %{version}
}@

@{package gb.db2.sqlite3
BuildRequires:  pkgconfig(sqlite3)
Requires:       %{name}-gb-db2 = %{version}
}@

@{package gb.dbus
BuildRequires:  pkgconfig(dbus-1)
}@

@{package gb.desktop
Requires:       %{name}-gb-desktop-x11 = %{version}
Requires:       %{name}-gb-image = %{version}
Requires:       %{name}-runtime = %{version}
Requires:       xdg-utils
Recommends:     libsecret-tools
}@

@{package gb.desktop.gnome.keyring
BuildRequires:  pkgconfig(gnome-keyring-1)
Requires:       %{name}-gb-desktop = %{version}
Provides:       %{name}-gb-desktop-gnome = %{version}
Obsoletes:      %{name}-gb-desktop-gnome < %{version}
}@

@{package gb.desktop.x11
BuildRequires:  pkgconfig(xtst)
Requires:       %{name}-gb-desktop = %{version}
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.gmp
BuildRequires:  gmp-devel
}@

@{package gb.gsl
BuildRequires:  pkgconfig(gsl)
}@

@{package gb.gtk
BuildRequires:  pkgconfig(fribidi)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  pkgconfig(gtkglext-1.0)
BuildRequires:  pkgconfig(ice)
BuildRequires:  pkgconfig(librsvg-2.0)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(sm)
Requires:       %{name}-gb-image = %{version}
Provides:       %{name}-gui = %{version}
}@

@{package gb.gtk3
BuildRequires:  pkgconfig(fribidi)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(gdk-wayland-3.0)
BuildRequires:  pkgconfig(gdk-x11-3.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(gtkglext-1.0)
BuildRequires:  pkgconfig(ice)
BuildRequires:  pkgconfig(librsvg-2.0)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(sm)
Requires:       %{name}-gb-image = %{version}
Provides:       %{name}-gb-gtk3-wayland = %{version}
Obsoletes:      %{name}-gb-gtk3-wayland < %{version}
Provides:       %{name}-gb-gtk3-x11 = %{version}
Obsoletes:      %{name}-gb-gtk3-x11 < %{version}
Provides:       %{name}-gui = %{version}
}@

@{package gb.gtk3.webview
BuildRequires:  pkgconfig(webkit2gtk-4.1)
Requires:       %{name}-gb-gtk3 = %{version}
}@

@{package gb.image.imlib
BuildRequires:  pkgconfig(imlib2)
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.image.io
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.libxml
BuildRequires:  pkgconfig(libxml-2.0)
}@

@{package gb.media
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(gstreamer-video-1.0)
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.mime
BuildRequires:  pkgconfig(gmime-3.0)
}@

%if %{with gb_mongodb}
@{package gb.mongodb
BuildRequires:  pkgconfig(libmongoc-1.0)
}@
%endif

@{package gb.ncurses
BuildRequires:  pkgconfig(ncurses)
}@

@{package gb.net.curl
BuildRequires:  pkgconfig(libcurl)
Requires:       %{name}-gb-net = %{version}
Requires:       %{name}-runtime = %{version}
}@

@{package gb.openal
BuildRequires:  pkgconfig(alure)
BuildRequires:  pkgconfig(openal)
}@

@{package gb.opengl
BuildRequires:  pkgconfig(glew)
}@

@{package gb.openssl
BuildRequires:  pkgconfig(libssl)
}@

@{package gb.pcre
BuildRequires:  pkgconfig(libpcre2-8)
}@

@{package gb.pdf
BuildRequires:  pkgconfig(poppler)
}@

@{package gb.poppler
BuildRequires:  pkgconfig(poppler-cpp)
BuildRequires:  pkgconfig(poppler-glib)
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.qt5
BuildRequires:  pkgconfig(Qt5Core) >= 5.5.0
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Network)
BuildRequires:  pkgconfig(Qt5PrintSupport)
BuildRequires:  pkgconfig(Qt5Svg)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5X11Extras)
BuildRequires:  pkgconfig(Qt5Xml)
BuildRequires:  pkgconfig(x11-xcb)
Requires:       %{name}-gb-image = %{version}
Provides:       %{name}-gb-qt5-ext = %{version}
Obsoletes:      %{name}-gb-qt5-ext < %{version}
Provides:       %{name}-gb-qt5-wayland = %{version}
Obsoletes:      %{name}-gb-qt5-wayland < %{version}
Provides:       %{name}-gb-qt5-x11 = %{version}
Obsoletes:      %{name}-gb-qt5-x11 < %{version}
Provides:       %{name}-gui = %{version}
}@

@{package gb.qt5.opengl
BuildRequires:  pkgconfig(Qt5OpenGL)
Requires:       %{name}-gb-opengl = %{version}
Requires:       %{name}-gb-qt5 = %{version}
}@

%if %{with gb_qt5_webkit}
@{package gb.qt5.webkit
BuildRequires:  pkgconfig(Qt5WebKit)
BuildRequires:  pkgconfig(Qt5WebKitWidgets)
Requires:       %{name}-gb-qt5 = %{version}
}@
%endif

@{package gb.qt5.webview
BuildRequires:  pkgconfig(Qt5WebEngine)
Requires:       %{name}-gb-qt5 = %{version}
}@

@{package gb.qt6
BuildRequires:  pkgconfig(Qt6Core)
BuildRequires:  pkgconfig(Qt6Gui)
BuildRequires:  pkgconfig(Qt6Network)
BuildRequires:  pkgconfig(Qt6PrintSupport)
BuildRequires:  pkgconfig(Qt6Svg)
BuildRequires:  pkgconfig(Qt6Widgets)
BuildRequires:  pkgconfig(Qt6Xml)
Requires:       %{name}-gb-image = %{version}
Provides:       %{name}-gb-qt6-ext = %{version}
Obsoletes:      %{name}-gb-qt6-ext < %{version}
Provides:       %{name}-gb-qt6-wayland = %{version}
Obsoletes:      %{name}-gb-qt6-wayland < %{version}
Provides:       %{name}-gb-qt6-x11 = %{version}
Obsoletes:      %{name}-gb-qt6-x11 < %{version}
Provides:       %{name}-gui = %{version}
}@

@{package gb.qt6.opengl
BuildRequires:  pkgconfig(Qt6OpenGL)
BuildRequires:  pkgconfig(Qt6OpenGLWidgets)
Requires:       %{name}-gb-opengl = %{version}
Requires:       %{name}-gb-qt6 = %{version}
}@

@{package gb.qt6.webview
BuildRequires:  pkgconfig(Qt6WebEngineCore)
BuildRequires:  pkgconfig(Qt6WebEngineWidgets)
Requires:       %{name}-gb-qt6 = %{version}
}@

@{package gb.scanner
Requires:       %{name}-gb-form-dialog = %{version}
Requires:       %{name}-gb-form-mdi = %{version}
Requires:       %{name}-gb-form-print = %{version}
Requires:       %{name}-gb-image = %{version}
Requires:       %{name}-gb-settings = %{version}
Requires:       sane-backends
}@

@{package gb.sdl
BuildRequires:  pkgconfig(SDL_ttf)
BuildRequires:  pkgconfig(glew)
Requires:       %{name}-gb-image = %{version}
Requires:       %{name}-gb-image-io = %{version}
}@

@{package gb.sdl.sound
BuildRequires:  pkgconfig(SDL_mixer)
}@

@{package gb.sdl2
BuildRequires:  pkgconfig(SDL2_image)
BuildRequires:  pkgconfig(SDL2_ttf)
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.sdl2.audio
BuildRequires:  pkgconfig(SDL2_mixer)
}@

@{package gb.v4l
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  pkgconfig(libv4l2)
Requires:       %{name}-gb-image = %{version}
}@

@{package gb.xml.xslt
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(libxslt)
Requires:       %{name}-runtime = %{version}
Requires:       %{name}-gb-xml = %{version}
}@

@{packages}@

%prep
%autosetup -n gambas-%{version} -p1

# fix benchmark scripts shebang
sed -e 's|bin/python$|bin/python3|;s|bin/env gbs3|bin/gbs3|;s|bin/java.*|bin/cat|' \
    -e 's|, "python",|, "python3",|' -i benchmark/*

%build
%if 0%{?suse_version} < 1600
export CC="gcc-11"
export CXX="g++-11"
%endif

./reconf-all
GAMBAS_CONFIG_FAILURE=1 %configure -C \
	--disable-qtwebkit	\
	--disable-qtwebview	\
	--disable-qt4		\
	--disable-sqlite2	\
%if %{without gb_mongodb}
	--disable-mongodb	\
%endif
%if %{without gb_qt5_webkit}
	--disable-qt5webkit	\
%endif
	%{nil}

%make_build

%install
%make_install XDG_UTILS=""

# desktop
install -D -m 0644 -t %{buildroot}%{_datadir}/applications app/desktop/%{name}.desktop

# mime
install -D -m 0644 main/mime/application-x-%{name}.xml %{buildroot}%{_datadir}/mime/packages/x-%{name}.xml
install -m 0644 app/mime/application-x-gambasscript.xml %{buildroot}%{_datadir}/mime/packages/x-gambasscript.xml
install -m 0644 app/mime/application-x-gambasserverpage.xml %{buildroot}%{_datadir}/mime/packages/x-gambasserverpage.xml
install -D -m 0644 app/mime/application-x-gambasscript-48.png \
	%{buildroot}%{_datadir}/icons/hicolor/48x48/mimetypes/application-x-gambasscript.png
install -D -m 0644 app/mime/application-x-gambasserverpage-48.png \
	%{buildroot}%{_datadir}/icons/hicolor/48x48/mimetypes/application-x-gambasserverpage.png
install -D -m 0644 main/mime/application-x-%{name}-48.png \
	%{buildroot}%{_datadir}/icons/hicolor/48x48/mimetypes/application-x-%{name}.png
install -D -m 0644 main/mime/application-x-%{name}.png \
	%{buildroot}%{_datadir}/icons/hicolor/256x256/mimetypes/application-x-%{name}.png
install -D -m 0644 -t %{buildroot}%{_datadir}/icons/hicolor/256x256/mimetypes \
	app/mime/application-x-gambas{script,serverpage}.png

# icons
install -D -m 0644 app/desktop/%{name}-48.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
install -D -m 0644 -t %{buildroot}%{_datadir}/icons/hicolor/256x256/apps app/desktop/%{name}.png
install -D -m 0644 -t %{buildroot}%{_datadir}/icons/hicolor/scalable/apps app/desktop/%{name}.svg

# benchmark & examples
cp -a benchmark app/examples %{buildroot}%{_datadir}/%{name}

%check
export PATH=%{buildroot}%{_bindir}:$PATH
export GB_PATH=%{buildroot}%{_bindir}/gbx3
cd main/lib/test/gb.test
gbc3 . && gba3 . && gbx3 -T "@All good" . || :

%files
%license COPYING
%doc README.md

%files ide
%license COPYING app/src/%{name}/.hidden/font/LICENSE
%doc README.md
%dir %{_datadir}/%{name}
%{_bindir}/%{name}{,.gambas}
%{_datadir}/%{name}/template
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.{png,svg}
%attr(0644,root,root) %{_datadir}/metainfo/%{name}.appdata.xml
%{_mandir}/man1/%{name}.1%{?ext_man}
%exclude %{_datadir}/appdata

%files runtime
%license COPYING
%doc AUTHORS README
%{_bindir}/gb{r,x}3
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/info
%dir %{_libdir}/%{name}
%{_datadir}/%{name}/info/gb.{debug,geom,gui,hash,jit,test}.*
%{_datadir}/%{name}/info/gb.eval.{info,list}
%{_datadir}/%{name}/info/gb.{info,list}
%{_datadir}/icons/hicolor/*/mimetypes/application-x-%{name}.png
%{_datadir}/mime/packages/x-%{name}.xml
%attr(0644,root,root) %{_libdir}/%{name}/gb.component
%{_libdir}/%{name}/gb.{debug,draw,geom,gui,hash,jit,test}.*
%{_libdir}/%{name}/gb.eval.{component,la,so*}
%{_mandir}/man1/gb{r,x}3.1%{?ext_man}

%files scripter
%{_bindir}/gb{s,w}3
%{_bindir}/gbs3.gambas
%{_datadir}/icons/hicolor/*/mimetypes/application-x-gambas{script,serverpage}.png
%{_datadir}/mime/packages/x-gambas{script,serverpage}.xml
%{_mandir}/man1/gb{s,w}3.1%{?ext_man}

%files dev-tools
%{_bindir}/gb{a,c,h,i}3
%{_bindir}/gbh3.gambas
%{_mandir}/man1/gb{a,c,h,i}3.1%{?ext_man}

%files benchmark
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/benchmark

%files examples
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/examples

@{files}@

%changelog
